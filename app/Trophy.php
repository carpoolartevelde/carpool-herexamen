<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trophy extends Model
{
    protected $fillable = [
        'display_text',
        'description',
        'image'
    ];
}
