<?php

/* Created by Seppe Beelprez
 * Edited by:
 *           Wesley Vanbrabant
 *           - Added user_id and seats to fillable
 *
 * */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ride extends Model
{
    protected $fillable = [

        'start',
        'end',
        'published_at',
        'user_id',
        'seats',
        'time'
    ];

    protected $dates = ['published_at'];

    /**
     * @param $date
     * set published date to today
     */
    public function setPublishedAtAttribute($date)
    {
        $this->attributes['published_at'] = Carbon::parse($date);
    }

    /**
     * @param $date
     * @return string
     * pasre published_at date in the right format
     */
    public function getPublishedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * An ride is owned by a user
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
