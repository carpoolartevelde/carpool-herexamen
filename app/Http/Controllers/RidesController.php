<?php

/*
 * Created by: Seppe Beelprez
 * Edited by:
 *      Wesley Vanbrabant:
 *          - Created/Edited every function up to the point where they are now.
 *          - Added delete function
 *
 * */

namespace App\Http\Controllers;

use App\Ride;
use App\User;
use App\Http\Requests;
use App\Http\Requests\ShareRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;





/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class RidesController extends Controller
{
    /**
     *  Authorize before continuing
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        try{
            $statusCode = 200;
            $response = [
            ];

            $rides = Ride::all();

            foreach($rides as $ride){

                $response[] = [
                    'id'        => $ride->id,
                    'start'     => $ride->start,
                    'end'       => $ride->end,
                    'user_id'   => $ride->user_id,
                    'seats'     => $ride->seats,
                    'time'      => $ride->time
                ];
            }

        }catch (Exception $e) {
            $statusCode = 400;

        }{
        return JsonResponse::create($response);
    }

    }

    public function show($id){

        try{
            $ride = Ride::find($id);

            $response = [ "ride" => [
                'id'        =>  $id,
                'start'     =>  $ride->start,
                'end'       =>  $ride->end,
                'user_id'   =>  $ride->user_id,
                'seats'     => $ride->seats,
                'time'      => $ride->time
            ]
            ];

        }catch(Exception $e){
            $response = [
                "error" => "File doesn`t exists"
            ];
        }
        return JsonResponse::create($response);

    }


    public function store(Request $request)
    {
        $id = DB::table('rides')->max('id');
        $start = $request->input('start');
        $end = $request->input('end');
        $seats = $request->input('seats');
        $time = $request->input('time');


        $ride = Ride::create(array(
            'id'        => $id + 1,
            'start'     => $start,
            'end'       => $end,
            'user_id'   => Auth::user()->id,
            'seats'     => $seats,
            'time'      => $time
        ));

        Auth::user()->rides()->save($ride);

    }

    public function destroy($id)
    {
        $ride = Ride::find($id);

        $ride->delete();
    }


    public function index2()
    {
        $users = User::all();
        return $users;
    }

    public function userId(){
        $user_id = Auth::user()->id;

        return $user_id;
    }
}