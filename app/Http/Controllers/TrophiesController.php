<?php

namespace App\Http\Controllers;

use App\Trophy;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;


class TrophiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try{
            $statusCode = 200;
            $response = [
            ];

            $trophies = Trophy::all();

            foreach($trophies as $trophy){

                $response[] = [
                    'id'            => $trophy->id,
                    'user_id'       => $trophy->user_id,
                    'display_text'  => $trophy->display_text,
                    'description'   => $trophy->description,
                    'image'         => $trophy->image
                ];
            }

        }catch (Exception $e) {
            $statusCode = 400;

        }{
        return JsonResponse::create($response);
    }    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
