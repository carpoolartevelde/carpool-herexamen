<?php

namespace App\Http\Controllers;

use App\Ride;
use App\Http\Requests;
use App\Http\Requests\ShareRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\HttpResponse;
use App\Http\Controllers\Controller;
use Carbon\Carbon;


/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     *  Authorize before continuing
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('app');
    }

}
