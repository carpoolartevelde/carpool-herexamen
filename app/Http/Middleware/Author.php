<?php

namespace App\Http\Middleware;

use App\Ride;
use Closure;
use Illuminate\Support\Facades\Auth;

class Author
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ride = Ride::find($request);

        if ($ride->user_id != Auth::user()->id)
        {
            // abort
        }

        return $next($request);
    }
}
