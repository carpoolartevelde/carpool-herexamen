<?php

/*
 * File created by Laravel itself
 * Adjusted by: Seppe Beelprez (created routing for RidesController/DashboardController - API group)
 * Adjusted by: Wesley Vanbrabant (Created route for users, etc - adjusted API routing to include versioning)
 *
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('/', array('as' => 'dashboard', 'uses' => 'DashboardController@dashboard'));
//Route::get('/dashboard2', array('as' => 'dashboard2', 'uses' => 'DashboardController@dashboard2'));


//Route::resource('/', 'DashboardController');
//Route::resource('rides', 'RidesController');


// Settings Routes
// ===================


Route::resource('/', 'DashboardController');

Route::group(array('prefix' => 'api/1.0'), function() {

    Route::resource('rides', 'RidesController');
    
    Route::resource('cities', 'CitiesController',
        array('only' => array('index')));

    Route::resource('trophies', 'TrophiesController',
        array('only' => array('index')));


    Route::resource('user_id', 'RidesController@userId');
    Route::resource('users', 'RidesController@index2');

});

// Auth Routes
// ===================
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);


