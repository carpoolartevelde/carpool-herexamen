;(function(){
    "use strict";

   var app = angular.module('app',
        [
            'app.controllers',
            'app.filters',
            'app.services',
            'app.directives',
            'app.routes',
            'app.config'
        ]);

    angular.module('app.routes', ['ui.router', 'ngStorage', 'ngMaterial', 'restangular']);
    angular.module('app.controllers', ['ui.router', 'ngMaterial', 'ngStorage', 'restangular']);
    angular.module('app.filters', []);
    angular.module('app.services', ['ui.router', 'ngStorage', 'restangular']);
    angular.module('app.directives', []);
    angular.module('app.config', []);

})()
/*
* File created by: Seppe Beelprez
* Edited by:
*           Wesley Vanbrabant: - view for specific edit/update ride
*
* */
;(function(){
    "use strict";

    angular.module('app.routes').config( ["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        var getView = function( viewName ){
            return '/views/' + viewName + '.html';
        };

        $urlRouterProvider.otherwise('/');

        $stateProvider

            .state('landing', {
                url: '/',
                views: {
                    main: {
                        templateUrl: getView('dashboard/index')
                    }
                }
            })
            .state('dashboard', {
                url: '/dashboard',
                views: {
                    main: {
                        templateUrl: getView('dashboard/index')
                    }
                }
            })
            .state('rides', {
                url: '/rides',
                views: {
                    main: {
                        templateUrl: getView('rides/index')
                    }
                }
            })
            .state('edit', {
                url: '/rides/edit/:id',
                views: {
                    main: {
                        templateUrl: getView('rides/edit/:id')
                    }
                }
            })
            .state('trophies', {
                url: '/trophies',
                views: {
                    main: {
                        templateUrl: getView('trophies/index')
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    main: {
                        templateUrl: getView('settings/index')
                    }
                }
            })
            .state('about', {
                url: '/settings/about',
                views: {
                    main: {
                        templateUrl: getView('settings/about')
                    }
                }
            })
            .state('profile', {
                url: '/settings/profile',
                views: {
                    main: {
                        templateUrl: getView('settings/profile')
                    }
                }
            })
            .state('login', {
                url: '/auth/login',
                views: {
                    main: {
                        templateUrl: getView('auth/login')
                    }
                }
            });


    }] );
})()
/*
 * File: Created by Seppe Beelprez
 *
 *  Added by Wesley Vanbrabant:
 *   - MainController scope-directives (f.e. $scope.ridesApi)
 *   - Scope functions (sendData) sendData
 *
 *  Added by Seppe Beelprez
 *  - Scope function (sendData) -> via service (save.factory.js)
 *
 *   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
 *   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
 */
;(function () {

    angular.module('app.controllers')
        .controller('MainController', MainController);

    MainController.$inject = [
        '$scope',
        '$http',
        // Custom
        'SaveFactory',
        'UserIdFactory'
    ];

    function MainController(
        $scope,
        $http,
        // Custom
        SaveFactory,
        UserIdFactory
        ) {

        $scope.createRide = {};

        //Scope variables (MainController)

        $scope.ridesApi = 'api/1.0/rides';
        $scope.start = '';
        $scope.end = '';
        $scope.seats = '';
        $scope.time = '';
        $scope.userId = 'api/1.0/user_id';
        $scope.totalSeats = ('1 2 3 4 5').split(' ')
            .map(function (seats) {
                return { abbrev: seats }
            });


        //Angular Ajax calls (MainController)
        //Get current user Id
        UserIdFactory.getUserId($scope);


        //Functions (MainController)
        $scope.sendData = function(){

            //SaveFactory.save($scope);

            if($scope.form.$valid) {
                SaveFactory.save($scope);
            }

            console.log("Startlocation is: " + $scope.start + ".");
            console.log("Endlocation is: " + $scope.end + ".");
            console.log("Amount of seats is: " + $scope.seats + ".");
            console.log("Time is: " + $scope.time + ".");

        };

        $scope.reset = function(form) {
            if (form) {
                form.$setPristine();
                form.$setUntouched();
            }
            $scope.user = angular.copy($scope.master);
        };

        $scope.reset();

    }

})();




/*
 * File: Created by Seppe Beelprez
 *
 *  Added by Wesley Vanbrabant:
 *   - MainController scope-directives (f.e. $scope.ridesApi)
 *   - Scope functions (sendData) sendData
 *
 *  Added by Seppe Beelprez
 *  - Scope function (sendData) -> via save.factory.js
 *
 *   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
 *   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
 */
;(function () { 'use strict';

    angular.module('app.controllers')
        .controller('SearchRidesController', SearchRidesController);

    SearchRidesController.$inject = [
        '$scope',
        '$http',
        // Custom
        'GetRidesFactory',
        'UserIdFactory'
    ];

    function SearchRidesController(
        $scope,
        $http,
        // Custom
        GetRidesFactory,
        UserIdFactory
        ) {

        //scope variables

        $scope.ridesApi = 'api/1.0/rides';
        $scope.userId = 'api/1.0/user_id';


        //Angular Ajax calls (SearchRidesController)

        GetRidesFactory.getRides($scope);

        UserIdFactory.getUserId($scope);

    }

})()

/* File: Created by Seppe Beelprez
*
*  Added by Wesley Vanbrabant:
*   - Function for delete
*   - Function for update/edit screen
**

*/
;(function () {

    angular.module('app.controllers')
        .controller('OverviewController', OverviewController);

    OverviewController.$inject = [
        '$scope',
        '$http',
        '$location',
        // Custom
        'GetRidesFactory',
        'UserIdFactory',
        'DeleteRide',
        'SaveFactory'
    ];

    function OverviewController(
        $scope,
        $http,
        $location,
        // Custom
        GetRidesFactory,
        UserIdFactory,
        DeleteRide,
        SaveFactory
        ) {

        //scope variables (OverviewController)

        $scope.ridesApi = 'api/1.0/rides';
        $scope.userId = 'api/1.0/user_id';

        $scope.goUpdate = function(id){
            SaveFactory.update(id)
                .then(function(){
                    console.log(id);//Gets the ID of the ride that needs to be edited.
                    $location.path('rides/edit/' + id);
                    console.log(":)");
                },function(){
                    console.log(":(");
                });
        };


        //Angular Ajax calls (OverviewController)
        UserIdFactory.getUserId($scope);

        GetRidesFactory.getRides($scope);

        $scope.remove = function(id) {
            DeleteRide.destroy(id)
                .then(function(data) {
                    $scope.id = data;
                }, function(){
                    $location.path('/rides');
                })
        };
    }

})()



///*
//* File: Created by Seppe Beelprez
//*
//*  Added by Wesley Vanbrabant:
//*   - MainController scope-directives (f.e. $scope.ridesApi)
//*   - Scope functions (sendData) sendData
//*   - Entire OverviewController
//*
//*   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
//*   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
// */
//;(function () { 'use strict';
//
//    angular.module('app.controllers', [])
//        .controller('MainController', ['$scope', '$http', function($scope, $http){
//
//
//            //Scope variables (MainController)
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.start = '';
//            $scope.end = '';
//            $scope.seats = '';
//            $scope.userId = 'api/1.0/user_id';
//            $scope.totalSeats = ('1 2 3 4 5').split(' ')
//                .map(function (seats) {
//                    return { abbrev: seats }
//                });
//
//
//            //Angular Ajax calls (MainController)
//            //Get current user Id
//            $http.get($scope.userId)
//                .then (function(data){
//                    $scope.gottenId = data.data;
//                    console.log("Get user current ID worked!" + $scope.gottenId);
//                }, function(){
//                    console.log("Get user current ID failed");
//                }
//            );
//
//
//            //Functions (MainController)
//
//            $scope.sendData = function(){
//                $http.post('api/1.0/rides', {'start': $scope.start, 'end': $scope.end, 'seats': $scope.seats})
//                    .then (function(data, status, headers, config){
//                        console.log ("data sent to API, new object created");
//                    },
//                    function(){
//                        console.log("data not sent to API, new object is not created");
//                    });
//
//                console.log("Startlocation is: " + $scope.start + ".");
//                console.log("Endlocation is: " + $scope.end + ".");
//                console.log("Amount of seats is: " + $scope.seats + ".");
//
//
//            };
//
//            $scope.reset = function(form) {
//                if (form) {
//                    form.$setPristine();
//                    form.$setUntouched();
//                }
//                $scope.user = angular.copy($scope.master);
//            };
//
//            $scope.reset();
//        }])
//
//
//        .controller('OverviewController', ['$http', '$scope', function($http, $scope){
//
//
//            //scope variables (OverviewController)
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.userId = 'api/1.0/user_id';
//
//
//            //Angular Ajax calls (OverviewController)
//
//            $http.get($scope.userId)
//                .then (function(data){
//                    $scope.gottenId = data.data;
//                    console.log($scope.gottenId)
//                }, function(){
//                    console.log("hey, get userId werkt nie!");
//                }
//            );
//
//
//            $http.get($scope.ridesApi)
//                .then (function(data){
//                console.log(data.data);
//                $scope.rides = data.data;
//                // console.log($scope.rides);
//
//
//            }, function(){
//                console.log('hey, get werkt totaal nie');
//
//            })
//
//        }])
//
//        .controller('SearchRidesController', ['$http', '$scope', function($http, $scope){
//
//            //scope variables
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.userId = 'api/1.0/user_id';
//
//
//            //Angular Ajax calls (SearchRidesController)
//
//            $http.get($scope.ridesApi)
//                .then (function(data){
//                $scope.rides = data.data;
//            }, function(){
//                console.log('hey, get werkt totaal nie');
//            })
//
//
//
//        }])
//})();
//
//

/**
 * Created by vanbrabantwesley on 16/08/15.
 */
/* File: Created by Wesley Vanbrabant
 *
 *  Added by Wesley Vanbrabant:
 *  Everything
 *
 *
 */
;(function () {

    angular.module('app.controllers')
        .controller('TrophiesController', TrophiesController);

    TrophiesController.$inject = [
        '$scope',
        '$http',
        // Custom
        'GetTrophiesFactory',
        'UserIdFactory'
    ];

    function TrophiesController(
        $scope,
        $http,
        // Custom
        GetTrophiesFactory,
        UserIdFactory
        ) {

        //scope variables (OverviewController)

        $scope.trophiesApi = 'api/1.0/trophies';
        $scope.userId = 'api/1.0/user_id';



        //Angular Ajax calls (OverviewController)

        UserIdFactory.getUserId($scope);


        GetTrophiesFactory.getTrophies($scope);

    }

})()



/*
 * Created by Wesley Vanbrabant
 *
*/

;(function () { 'use strict';

    angular.module('app.services')
        .factory('UserIdFactory', UserIdFactory);

    UserIdFactory.$inject = [
        '$http'

    ];

    function UserIdFactory(

        $http

        ) {

        return {

            getUserId : function(GetUserId) {
                $http.get(GetUserId.userId)
                    .then (function(data){
                        GetUserId.gottenId = data.data;
                        console.log(GetUserId.gottenId)
                    }, function(){
                        console.log("hey, get userId werkt nie!");
                    }
                );
            }

        }

    }

})();

/*
 * Created by Wesley Vanbrabant
 * Made
 * */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('DeleteRide', DeleteRide);

    DeleteRide.$inject = [
        '$http'
    ];

    function DeleteRide(

        $http

        ) {

        return {

            destroy : function(id) {
                return $http.delete('/api/1.0/rides/' + id);
            }

        }

    }

})();

/*
* Created by Wesley Vanbrabant
* */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('GetRidesFactory', GetRidesFactory);

    GetRidesFactory.$inject = [
        '$http'

    ];

    function GetRidesFactory(

        $http

        ) {

        return {

            getRides : function(GetRides) {
                $http.get(GetRides.ridesApi)
                    .then (function(data){
                        GetRides.rides = data.data;
                        console.log("get rides data werkt");
                        console.log(GetRides.rides);
                    },
                    function(){
                        console.log("get rides data werkt niet");
                    });
            }

        }

    }

})();

;(function () { 'use strict';

    angular.module('app.services')
        .factory('SaveFactory', SaveFactory);

    SaveFactory.$inject = [
        '$http'
    ];

    function SaveFactory(

        $http

        ) {

        return {

            save : function(CreateRide) {
                $http.post('api/1.0/rides', {'start': CreateRide.start, 'end': CreateRide.end, 'seats': CreateRide.seats, 'time': CreateRide.time})
                    .then (function(data, status, headers, config){
                        console.log ("data sent to API, new object created");
                    },
                    function(){
                        console.log("data not sent to API, new object is not created");
                    });
            },

            update : function(id) {
                return $http.get('api/1.0/rides/' + id);
            }

        }

    }

})();

/*
 * Created by Wesley Vanbrabant
 * Created: - Function getTrophies (call to server)
 *          - Service itself.
 * */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('GetTrophiesFactory', GetTrophiesFactory);

    GetTrophiesFactory.$inject = [
        '$http'

    ];

    function GetTrophiesFactory(

        $http

        ) {

        return {

            getTrophies : function(GetTrophies) {
                $http.get(GetTrophies.trophiesApi)
                    .then (function(data){
                        GetTrophies.trophies = data.data;
                        console.log("get trophies data werkt");
                        console.log(GetTrophies.trophies);
                    },
                    function(){
                        console.log("get trophies data werkt niet");
                    });
            }

        }

    }

})();

