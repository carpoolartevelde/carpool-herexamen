
    <div id="header">
        <div class="row header-wrapper">
            <ul>
                <li class=" col-xs-3 col-sm-3 col-md-3 col-lg-3">
                     <a href="#/" class="menu"><i class="fa fa-tachometer fa-2x"></i></a>
                 </li>
                 <li class="menu col-xs-3 col-sm-3 col-md-3 col-lg-3">
                     <a href="#/rides"><i class="fa fa-car fa-2x"></i></a>
                 </li>
                 <li class="menu col-xs-3 col-sm-3 col-md-3 col-lg-3">
                     <a href="#/trophies"><i class="fa fa-trophy fa-2x"></i></a>
                 </li>
                 <li class="menu col-xs-3 col-sm-3 col-md-3 col-lg-3">
                     <a href="#/settings"><i class="fa fa-gear fa-2x"></i></a>
                 </li>
            </ul>
        </div>
    </div>