<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
	<meta charset="UTF-8">
	<title>frontoffice.blade.php</title>
	<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css"> <!-- load bootstrap via cdn -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"> <!-- load fontawesome -->
    <style>
        body        { padding-top:30px; }
        form        { padding-bottom:20px; }
        .comment    { padding-bottom:20px; }
    </style>

    <!-- JS -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.8/angular.min.js"></script> <!-- load angular -->

    <!-- ANGULAR -->
    <!-- all angular resources will be loaded from the /public folder -->
        <script src="../../assets/js/frontoffice/controllers/mainCtrl.js"></script> <!-- load our controller -->
        <script src="../../assets/js/frontoffice/services/commentService.js"></script> <!-- load our service -->
        <script src="../../assets/js/frontoffice/app.js"></script> <!-- load our application -->

</head>
<!-- declare our angular app and controller -->
<body>
    <div class="container">
		<div class="row">
		    @yield('content')
		</div>
	</div>
</body>
</html>