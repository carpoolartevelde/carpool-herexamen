@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Dashboard Blade</h3>
</md-toolbar>

<div id="dashboard-content" layout="row" layout-sm="column" layout-margin>
    <div class="big-button" flex >
        <h4><a href="share">Share rides</a></h4>
    </div>
    <div class="big-button" flex>
        <h4>Search rides</h4>
    </div>
</div>



                            {{--{!! Form::model($ride = new \App\Ride, ['url' => 'dashboard2']) !!}--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! Form::label('start', 'Start Point') !!}--}}
                                    {{--{!! Form::text('start', null, ['class' => 'form-control']) !!}--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! Form::label('end', 'End Point') !!}--}}
                                    {{--{!! Form::text('end', null, ['class' => 'form-control']) !!}--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}--}}
                                {{--</div>--}}

                            {{--{!! Form::close() !!}--}}

{{--<md-content class="md-padding">--}}
    {{--<md-tabs md-dynamic-height md-border-bottom>--}}
        {{--<md-tab label="Share Rides">--}}
            {{--<md-content class="md-padding">--}}
                {{--<div ng-controller="FormExampleController">--}}
                    {{--<form name="form" class="css-form" role="form" method="post" action="{{ url('/dashboard2') }}" novalidate>--}}
                        {{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}

                                {{--<label>Start Point</label>--}}
                                {{--<input type="text" ng-model="share.start" name="shareStart" required="">--}}
                                {{--<ng-messages>--}}
                                    {{--<div ng-show="form.$submitted || form.shareStart.$touched">--}}
                                        {{--<div ng-show="form.shareStart.$error.required">Where do you start?</div>--}}
                                    {{--</div>--}}
                                {{--</ng-messages>--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}
                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}
                                {{--<label>End Point</label>--}}
                                {{--<input type="text" ng-model="share.end" name="shareEnd" required="">--}}
                                {{--<ng-messages>--}}
                                    {{--<div ng-show="form.$submitted || form.shareEnd.$touched">--}}
                                        {{--<div ng-show="form.shareEnd.$error.required">Where do you end?</div>--}}
                                    {{--</div>--}}
                                {{--</ng-messages>--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}
                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}
                                {{--<md-button class="gen_button" ng-click="update(share)"> Create </md-button>--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</md-content>--}}

            {{--<md-content>--}}
                {{--@foreach($rides as $ride)--}}
                    {{--<article>--}}
                        {{--<h3>{{ $ride->start }}</h3>--}}

                        {{--<p>{{ $ride->end }}</p>--}}
                    {{--</article>--}}
                {{--@endforeach--}}
            {{--</md-content>--}}
        {{--</md-tab>--}}
        {{--<md-tab label="Search Rides">--}}
            {{--<md-content class="md-padding">--}}

            {{--</md-content>--}}
        {{--</md-tab>--}}
    {{--</md-tabs>--}}
{{--</md-content>--}}
@stop