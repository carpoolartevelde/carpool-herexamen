@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Share a ride</h3>
</md-toolbar>



<md-content class="md-padding">
    <div ng-controller="FormExampleController">
        {!! Form::open(['url' => 'share', 'name' => 'shareForm', 'class' => 'css-form']) !!}

            {{--@include('pages.forms.ride', ['submitButtonText' => 'Create Ride'])--}}

            <div class="form-group">

                {!! Form::label('start', 'Start Point') !!}
                {!! Form::text('start', null, ['class' => 'form-control'], ['ng-model' => 'share.start'], ['required' => '']) !!}
                {{--<ng-messages>--}}
                    {{--<div ng-show="form.$submitted || form.shareStart.$touched">--}}
                        {{--<div ng-show="form.shareStart.$error.required">Where do you start?</div>--}}
                    {{--</div>--}}
                {{--</ng-messages>--}}
            </div>

            <div class="form-group">
                {!! Form::label('end', 'End Point') !!}
                {!! Form::text('end', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <md-button class="gen_button" ng-click="update(user)" value="Create"></md-button>
                {!! Form::submit('Create', ['class' => 'btn gen_button']) !!}
            </div>

        {!! Form::close() !!}

                {{--<form name="form" class="css-form" novalidate>--}}

                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}
                                {{--<label>Start Point</label>--}}
                                {{--<input type="text" ng-model="share.start" name="shareStart" required="">--}}
                                {{--{!! Form::text('start', null, ['class' => 'form-control']) !!}--}}
                                {{--<ng-messages>--}}
                                    {{--<div ng-show="form.$submitted || form.shareStart.$touched">--}}
                                        {{--<div ng-show="form.shareStart.$error.required">Where do you start?</div>--}}
                                    {{--</div>--}}
                                {{--</ng-messages>--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}
                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}
                                {{--<label>End Point</label>--}}
                                {{--<input type="text" ng-model="share.end" name="shareEnd" required="">--}}
                                {{--<ng-messages>--}}
                                    {{--<div ng-show="form.$submitted || form.shareEnd.$touched">--}}
                                        {{--<div ng-show="form.shareEnd.$error.required">Where do you end?</div>--}}
                                    {{--</div>--}}
                                {{--</ng-messages>--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}
                        {{--<div layout layout-sm="column">--}}
                            {{--<md-input-container flex-gt-md="33" offset-gt-md="33" flex-gt-sm="50" offset-gt-sm="25">--}}
                                {{--<md-button class="gen_button" ng-click="update(share)"> Create </md-button>--}}
                                {{--{!! Form::submit('Create', ['class' => 'btn btn-primary gen_button']) !!}--}}
                            {{--</md-input-container>--}}
                        {{--</div>--}}

                    {{--</form>--}}
                {{--</div>--}}
    </div>
</md-content>

@stop