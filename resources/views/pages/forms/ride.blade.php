
    <div class="form-group">
        {!! Form::label('start', 'Start Point') !!}
        {!! Form::text('start', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('end', 'End Point') !!}
        {!! Form::text('end', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
    </div>
