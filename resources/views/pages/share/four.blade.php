@extends('headermenu')


@section('content')

@if ($success == true)
    <div class="content-container">
        <div class="row">
            <div class="col-xs-12">
                <p class="share-success">Your ride was shared successfully!</p>
            </div>

            <div class="col-xs-offset-2">
                <button class="btn btn-return-home col-xs-10">Return Home</button>
            </div>
         </div>
     </div>

    @else

    <p></p>

@endif

@stop
