@extends('headermenu')


@section('content')
<div class="content-container">
    <div class="row">
        <p class="steps_title">Step 1/3</p>
    </div>

    <div class="row">
        <p class="steps_subtitle col-xs-offset-1 col-sm-offset-1">Where do you launch</p>
    </div>


    {!! Form::open() !!}
    <div class="form-group txt-input">
      <div class="col-lg-1 col-xs-1 col-xs-offset-1">
            <i class="fa fa-map-marker fa-2x"></i>
      </div>

      <div class="col-lg-9 col-xs-10">
            {!! Form::text('task', null, ['class' => 'form-control', 'placeholder' => 'Start location']) !!}
      </div>
    </div>

    <div class="form-group txt-input">
      <div class="col-lg-1 col-xs-1 col-xs-offset-1">
            <i class="fa fa-map-marker fa-2x"></i>
      </div>

      <div class="col-lg-9 col-xs-10 ">
            {!! Form::text('task', null, ['class' => 'form-control', 'placeholder' => 'End location']) !!}
      </div>
    </div>
     {!! Form::close() !!}


    <div class="row">
        <div class="col-xs-offset-9">
        {!!Form::button('Next', ['class' => 'btn button-next']) !!}
        </div>
    </div>

 </div>


@stop