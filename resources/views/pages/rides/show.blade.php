@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Ride Specific</h3>
</md-toolbar>



<md-content class="md-padding">
    <article>
        <h3>{{ $ride->start }}</h3>
        <p>to</p>
        <h3>{{ $ride->end }}</h3>
        <p>{{ $ride->id }}</p>
        <a href="{{ action('RidesController@edit', [$ride->id]) }}">Edit</a>


        {{--<p>{{ $ride->user_id }}</p>--}}
    </article>
</md-content>

@stop