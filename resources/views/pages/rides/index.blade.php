@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Rides</h3>
</md-toolbar>

<md-content class="md-padding">
    <md-tabs md-dynamic-height md-border-bottom>
        <md-tab label="Own Rides">
            <md-content class="md-padding">
                <md-content>
                    @foreach($rides as $ride)
                        <article>
                            <h3>{{ $ride->start }}</h3>

                            <a href="{{ action('RidesController@show', [$ride->id]) }}">{{ $ride->end }}</a>

                            <p>{{ $author->name }}</p>
                        </article>
                    @endforeach
                </md-content>
            </md-content>
        </md-tab>
        <md-tab label="Other Rides">
            <md-content class="md-padding">

            </md-content>
        </md-tab>
    </md-tabs>
</md-content>
@stop