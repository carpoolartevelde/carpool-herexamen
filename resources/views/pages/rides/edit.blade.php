@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Edit a ride</h3>
</md-toolbar>



<md-content class="md-padding">
    {!! Form::model($ride, ['method' => 'PATCH', 'action' => ['RidesController@update', $ride->id]]) !!}

        @include('pages.forms.ride', ['submitButtonText' => 'Edit Ride'])

    {!! Form::close() !!}
</md-content>

@stop