@extends('layouts.frontoffice')


@section('content')

<md-toolbar class="md-toolbar-overwrite">
    <h3 class="md-subhead">Settings</h3>
</md-toolbar>

    <div class="col-lg-12">
        <div>
            <h2><a href="/settings/about">about</a></h2>
        </div>

        <div>
            <h2><a href="/auth/logout">logout</a></h2>
        </div>
    </div>
@stop