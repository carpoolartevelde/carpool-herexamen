/**
 * Created by vanbrabantwesley on 16/08/15.
 */
/* File: Created by Wesley Vanbrabant
 *
 *  Added by Wesley Vanbrabant:
 *  Everything
 *
 *
 */
;(function () {

    angular.module('app.controllers')
        .controller('TrophiesController', TrophiesController);

    TrophiesController.$inject = [
        '$scope',
        '$http',
        // Custom
        'GetTrophiesFactory',
        'UserIdFactory'
    ];

    function TrophiesController(
        $scope,
        $http,
        // Custom
        GetTrophiesFactory,
        UserIdFactory
        ) {

        //scope variables (OverviewController)

        $scope.trophiesApi = 'api/1.0/trophies';
        $scope.userId = 'api/1.0/user_id';



        //Angular Ajax calls (OverviewController)

        UserIdFactory.getUserId($scope);


        GetTrophiesFactory.getTrophies($scope);

    }

})()


