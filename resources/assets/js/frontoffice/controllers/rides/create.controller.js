/*
 * File: Created by Seppe Beelprez
 *
 *  Added by Wesley Vanbrabant:
 *   - MainController scope-directives (f.e. $scope.ridesApi)
 *   - Scope functions (sendData) sendData
 *
 *  Added by Seppe Beelprez
 *  - Scope function (sendData) -> via service (save.factory.js)
 *
 *   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
 *   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
 */
;(function () {

    angular.module('app.controllers')
        .controller('MainController', MainController);

    MainController.$inject = [
        '$scope',
        '$http',
        // Custom
        'SaveFactory',
        'UserIdFactory'
    ];

    function MainController(
        $scope,
        $http,
        // Custom
        SaveFactory,
        UserIdFactory
        ) {

        $scope.createRide = {};

        //Scope variables (MainController)

        $scope.ridesApi = 'api/1.0/rides';
        $scope.start = '';
        $scope.end = '';
        $scope.seats = '';
        $scope.time = '';
        $scope.userId = 'api/1.0/user_id';
        $scope.totalSeats = ('1 2 3 4 5').split(' ')
            .map(function (seats) {
                return { abbrev: seats }
            });


        //Angular Ajax calls (MainController)
        //Get current user Id
        UserIdFactory.getUserId($scope);


        //Functions (MainController)
        $scope.sendData = function(){

            //SaveFactory.save($scope);

            if($scope.form.$valid) {
                SaveFactory.save($scope);
            }

            console.log("Startlocation is: " + $scope.start + ".");
            console.log("Endlocation is: " + $scope.end + ".");
            console.log("Amount of seats is: " + $scope.seats + ".");
            console.log("Time is: " + $scope.time + ".");

        };

        $scope.reset = function(form) {
            if (form) {
                form.$setPristine();
                form.$setUntouched();
            }
            $scope.user = angular.copy($scope.master);
        };

        $scope.reset();

    }

})();



