///*
//* File: Created by Seppe Beelprez
//*
//*  Added by Wesley Vanbrabant:
//*   - MainController scope-directives (f.e. $scope.ridesApi)
//*   - Scope functions (sendData) sendData
//*   - Entire OverviewController
//*
//*   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
//*   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
// */
//;(function () { 'use strict';
//
//    angular.module('app.controllers', [])
//        .controller('MainController', ['$scope', '$http', function($scope, $http){
//
//
//            //Scope variables (MainController)
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.start = '';
//            $scope.end = '';
//            $scope.seats = '';
//            $scope.userId = 'api/1.0/user_id';
//            $scope.totalSeats = ('1 2 3 4 5').split(' ')
//                .map(function (seats) {
//                    return { abbrev: seats }
//                });
//
//
//            //Angular Ajax calls (MainController)
//            //Get current user Id
//            $http.get($scope.userId)
//                .then (function(data){
//                    $scope.gottenId = data.data;
//                    console.log("Get user current ID worked!" + $scope.gottenId);
//                }, function(){
//                    console.log("Get user current ID failed");
//                }
//            );
//
//
//            //Functions (MainController)
//
//            $scope.sendData = function(){
//                $http.post('api/1.0/rides', {'start': $scope.start, 'end': $scope.end, 'seats': $scope.seats})
//                    .then (function(data, status, headers, config){
//                        console.log ("data sent to API, new object created");
//                    },
//                    function(){
//                        console.log("data not sent to API, new object is not created");
//                    });
//
//                console.log("Startlocation is: " + $scope.start + ".");
//                console.log("Endlocation is: " + $scope.end + ".");
//                console.log("Amount of seats is: " + $scope.seats + ".");
//
//
//            };
//
//            $scope.reset = function(form) {
//                if (form) {
//                    form.$setPristine();
//                    form.$setUntouched();
//                }
//                $scope.user = angular.copy($scope.master);
//            };
//
//            $scope.reset();
//        }])
//
//
//        .controller('OverviewController', ['$http', '$scope', function($http, $scope){
//
//
//            //scope variables (OverviewController)
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.userId = 'api/1.0/user_id';
//
//
//            //Angular Ajax calls (OverviewController)
//
//            $http.get($scope.userId)
//                .then (function(data){
//                    $scope.gottenId = data.data;
//                    console.log($scope.gottenId)
//                }, function(){
//                    console.log("hey, get userId werkt nie!");
//                }
//            );
//
//
//            $http.get($scope.ridesApi)
//                .then (function(data){
//                console.log(data.data);
//                $scope.rides = data.data;
//                // console.log($scope.rides);
//
//
//            }, function(){
//                console.log('hey, get werkt totaal nie');
//
//            })
//
//        }])
//
//        .controller('SearchRidesController', ['$http', '$scope', function($http, $scope){
//
//            //scope variables
//
//            $scope.ridesApi = 'api/1.0/rides';
//            $scope.userId = 'api/1.0/user_id';
//
//
//            //Angular Ajax calls (SearchRidesController)
//
//            $http.get($scope.ridesApi)
//                .then (function(data){
//                $scope.rides = data.data;
//            }, function(){
//                console.log('hey, get werkt totaal nie');
//            })
//
//
//
//        }])
//})();
//
//
