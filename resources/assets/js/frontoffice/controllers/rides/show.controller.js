/* File: Created by Seppe Beelprez
*
*  Added by Wesley Vanbrabant:
*   - Function for delete
*   - Function for update/edit screen
**

*/
;(function () {

    angular.module('app.controllers')
        .controller('OverviewController', OverviewController);

    OverviewController.$inject = [
        '$scope',
        '$http',
        '$location',
        // Custom
        'GetRidesFactory',
        'UserIdFactory',
        'DeleteRide',
        'SaveFactory'
    ];

    function OverviewController(
        $scope,
        $http,
        $location,
        // Custom
        GetRidesFactory,
        UserIdFactory,
        DeleteRide,
        SaveFactory
        ) {

        //scope variables (OverviewController)

        $scope.ridesApi = 'api/1.0/rides';
        $scope.userId = 'api/1.0/user_id';

        $scope.goUpdate = function(id){
            SaveFactory.update(id)
                .then(function(){
                    console.log(id);//Gets the ID of the ride that needs to be edited.
                    $location.path('rides/edit/' + id);
                    console.log(":)");
                },function(){
                    console.log(":(");
                });
        };


        //Angular Ajax calls (OverviewController)
        UserIdFactory.getUserId($scope);

        GetRidesFactory.getRides($scope);

        $scope.remove = function(id) {
            DeleteRide.destroy(id)
                .then(function(data) {
                    $scope.id = data;
                }, function(){
                    $location.path('/rides');
                })
        };
    }

})()


