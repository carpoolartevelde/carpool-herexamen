/*
 * File: Created by Seppe Beelprez
 *
 *  Added by Wesley Vanbrabant:
 *   - MainController scope-directives (f.e. $scope.ridesApi)
 *   - Scope functions (sendData) sendData
 *
 *  Added by Seppe Beelprez
 *  - Scope function (sendData) -> via save.factory.js
 *
 *   @todo: (Added by Wesley Vanbrabant) when finished, put re-occuring/re-usable functions in services.
 *   Otherwise it really has no use to use an MVC structure, as the data object is in the presentation object
 */
;(function () { 'use strict';

    angular.module('app.controllers')
        .controller('SearchRidesController', SearchRidesController);

    SearchRidesController.$inject = [
        '$scope',
        '$http',
        // Custom
        'GetRidesFactory',
        'UserIdFactory'
    ];

    function SearchRidesController(
        $scope,
        $http,
        // Custom
        GetRidesFactory,
        UserIdFactory
        ) {

        //scope variables

        $scope.ridesApi = 'api/1.0/rides';
        $scope.userId = 'api/1.0/user_id';


        //Angular Ajax calls (SearchRidesController)

        GetRidesFactory.getRides($scope);

        UserIdFactory.getUserId($scope);

    }

})()
