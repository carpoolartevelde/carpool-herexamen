/*
* File created by: Seppe Beelprez
* Edited by:
*           Wesley Vanbrabant: - view for specific edit/update ride
*
* */
;(function(){
    "use strict";

    angular.module('app.routes').config( ["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        var getView = function( viewName ){
            return '/views/' + viewName + '.html';
        };

        $urlRouterProvider.otherwise('/');

        $stateProvider

            .state('landing', {
                url: '/',
                views: {
                    main: {
                        templateUrl: getView('dashboard/index')
                    }
                }
            })
            .state('dashboard', {
                url: '/dashboard',
                views: {
                    main: {
                        templateUrl: getView('dashboard/index')
                    }
                }
            })
            .state('rides', {
                url: '/rides',
                views: {
                    main: {
                        templateUrl: getView('rides/index')
                    }
                }
            })
            .state('edit', {
                url: '/rides/edit/:id',
                views: {
                    main: {
                        templateUrl: getView('rides/edit/:id')
                    }
                }
            })
            .state('trophies', {
                url: '/trophies',
                views: {
                    main: {
                        templateUrl: getView('trophies/index')
                    }
                }
            })
            .state('settings', {
                url: '/settings',
                views: {
                    main: {
                        templateUrl: getView('settings/index')
                    }
                }
            })
            .state('about', {
                url: '/settings/about',
                views: {
                    main: {
                        templateUrl: getView('settings/about')
                    }
                }
            })
            .state('profile', {
                url: '/settings/profile',
                views: {
                    main: {
                        templateUrl: getView('settings/profile')
                    }
                }
            })
            .state('login', {
                url: '/auth/login',
                views: {
                    main: {
                        templateUrl: getView('auth/login')
                    }
                }
            });


    }] );
})()