/*
 * Created by Wesley Vanbrabant
 *
*/

;(function () { 'use strict';

    angular.module('app.services')
        .factory('UserIdFactory', UserIdFactory);

    UserIdFactory.$inject = [
        '$http'

    ];

    function UserIdFactory(

        $http

        ) {

        return {

            getUserId : function(GetUserId) {
                $http.get(GetUserId.userId)
                    .then (function(data){
                        GetUserId.gottenId = data.data;
                        console.log(GetUserId.gottenId)
                    }, function(){
                        console.log("hey, get userId werkt nie!");
                    }
                );
            }

        }

    }

})();
