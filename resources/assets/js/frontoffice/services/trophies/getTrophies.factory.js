/*
 * Created by Wesley Vanbrabant
 * Created: - Function getTrophies (call to server)
 *          - Service itself.
 * */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('GetTrophiesFactory', GetTrophiesFactory);

    GetTrophiesFactory.$inject = [
        '$http'

    ];

    function GetTrophiesFactory(

        $http

        ) {

        return {

            getTrophies : function(GetTrophies) {
                $http.get(GetTrophies.trophiesApi)
                    .then (function(data){
                        GetTrophies.trophies = data.data;
                        console.log("get trophies data werkt");
                        console.log(GetTrophies.trophies);
                    },
                    function(){
                        console.log("get trophies data werkt niet");
                    });
            }

        }

    }

})();

