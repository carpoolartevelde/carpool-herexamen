;(function () { 'use strict';

    angular.module('app.services')
        .factory('SaveFactory', SaveFactory);

    SaveFactory.$inject = [
        '$http'
    ];

    function SaveFactory(

        $http

        ) {

        return {

            save : function(CreateRide) {
                $http.post('api/1.0/rides', {'start': CreateRide.start, 'end': CreateRide.end, 'seats': CreateRide.seats, 'time': CreateRide.time})
                    .then (function(data, status, headers, config){
                        console.log ("data sent to API, new object created");
                    },
                    function(){
                        console.log("data not sent to API, new object is not created");
                    });
            },

            update : function(id) {
                return $http.get('api/1.0/rides/' + id);
            }

        }

    }

})();
