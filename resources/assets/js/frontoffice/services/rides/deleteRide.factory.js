/*
 * Created by Wesley Vanbrabant
 * Made
 * */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('DeleteRide', DeleteRide);

    DeleteRide.$inject = [
        '$http'
    ];

    function DeleteRide(

        $http

        ) {

        return {

            destroy : function(id) {
                return $http.delete('/api/1.0/rides/' + id);
            }

        }

    }

})();
