/*
* Created by Wesley Vanbrabant
* */

;(function () { 'use strict';

    angular.module('app.services')
        .factory('GetRidesFactory', GetRidesFactory);

    GetRidesFactory.$inject = [
        '$http'

    ];

    function GetRidesFactory(

        $http

        ) {

        return {

            getRides : function(GetRides) {
                $http.get(GetRides.ridesApi)
                    .then (function(data){
                        GetRides.rides = data.data;
                        console.log("get rides data werkt");
                        console.log(GetRides.rides);
                    },
                    function(){
                        console.log("get rides data werkt niet");
                    });
            }

        }

    }

})();
