<?php
/*
 * Created by: Seppe Beelprez
 * Edited by:
 *      Wesley Vanbrabant:
 *          - Added DateTime to create schema
 *      Maxim Vanhove:
 *          - Added seats to create schema
 * */


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rides', function (Blueprint $table)
        {


            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('start');
            $table->string('end');
            $table->integer('seats')->unsigned();
            $table->dateTime('time');

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rides');
    }
}
