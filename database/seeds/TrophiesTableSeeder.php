<?php

use App\Trophy;
use Illuminate\Database\Seeder;

class TrophiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Trophy::create(array(
            'display_text' => "Trophy",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));

        Trophy::create(array(
            'display_text' => "Trophy 2",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));

        Trophy::create(array(
            'display_text' => "Trophy 3",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));

        Trophy::create(array(
            'display_text' => "Trophy 4",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));

        Trophy::create(array(
            'display_text' => "Trophy 5",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));

        Trophy::create(array(
            'display_text' => "Trophy 6",
            'description' => "Description",
            'image' => "http://vignette4.wikia.nocookie.net/thelastofus/images/1/10/Gold.png/revision/latest?cb=20130617213754"
        ));
    }
}
