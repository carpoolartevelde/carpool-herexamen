<?php

use Illuminate\Database\Seeder;
use App\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        City::create(array(
            'name' => "Gent",
            'postal_code' => 9000
        ));

        City::create(array(
            'name' => "Sint-Kruis-Winkel",
            'postal_code' => 9042
        ));

        City::create(array(
            'name' => "Oostakker",
            'postal_code' => 9041
        ));

        /*DB::table('cities')->insert([
            'name' => "Gent",
            'postal_code' => 9000
        ]);

        DB::table('cities')->insert([
            'name' => "Sint-Kruis-Winkel",
            'postal_code' => 9042
        ]);

        DB::table('cities')->insert([
            'name' => "Oostakker",
            'postal_code' => 9041
        ]);

        DB::table('cities')->insert([
            'name' => "Oostakker",
            'postal_code' => 9041
        ]);*/
    }
}
