<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(array(
            'name' => "Admin",
            'username' => "Admin",
            'email' => "admin@admin.com",
            'password' => '$2y$10$yI/3a9dik9f12ESIAYf.J.Osf4DXsKRv.qYTE3l6Wg14PHd8hTucy' //admin123
        ));
    }
}
